'use strict';

describe('Directive: pageNav', function () {

  // load the directive's module
  beforeEach(module('marvelViewerApp'));

  var element,
    scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<page-nav></page-nav>');
    element = $compile(element)(scope);
    expect(element.text()).toBe('this is the pageNav directive');
  }));
});
