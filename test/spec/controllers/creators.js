'use strict';

describe('Controller: CreatorsCtrl', function () {

  // load the controller's module
  beforeEach(module('marvelViewerApp'));

  var CreatorsCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    CreatorsCtrl = $controller('CreatorsCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
