'use strict';

describe('Controller: ComicsCtrl', function () {

  // load the controller's module
  beforeEach(module('marvelViewerApp'));

  var ComicsCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    ComicsCtrl = $controller('ComicsCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
