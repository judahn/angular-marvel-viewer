'use strict';

/**
 * @ngdoc overview
 * @name marvelViewerApp
 * @description
 * # marvelViewerApp
 *
 * Main module of the application.
 */
angular
  .module('marvelViewerApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch'
  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/comics.html',
        controller: 'ComicsCtrl'
      })
      .when('/characters', {
        templateUrl: 'views/characters.html',
        controller: 'CharactersCtrl'
      })
      .when('/creators', {
        templateUrl: 'views/creators.html',
        controller: 'CreatorsCtrl'
      })
      .when('/events', {
        templateUrl: 'views/events.html',
        controller: 'EventsCtrl'
      })
      .when('/series', {
        templateUrl: 'views/series.html',
        controller: 'SeriesCtrl'
      })
      .when('/stories', {
        templateUrl: 'views/stories.html',
        controller: 'StoriesCtrl'
      })
      .when('/about', {
        templateUrl: 'views/about.html',
        controller: 'AboutCtrl'
      })
      .otherwise({
        redirectTo: '/'
      });
  });
