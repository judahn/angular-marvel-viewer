'use strict';

/**
 * @ngdoc directive
 * @name marvelViewerApp.directive:pageNav
 * @description
 * # pageNav
 */
angular.module('marvelViewerApp')
  .directive('pageNav', function () {
    return {
      restrict: 'E',
      templateUrl: 'views/partials/page-nav.html'
    };
  });
