'use strict';

/**
 * @ngdoc directive
 * @name marvelViewerApp.directive:navigation
 * @description
 * # navigation
 */
angular.module('marvelViewerApp')
  .directive('navigation', function () {
    return {
      restrict: 'E',
      templateUrl: 'views/partials/navigation.html'
    };
  });
