'use strict';

/**
 * @ngdoc directive
 * @name marvelViewerApp.directive:pageHeader
 * @description
 * # pageHeader
 */
angular.module('marvelViewerApp')
  .directive('pageHeader', function () {
    return {
      restrict: 'E',
      templateUrl: 'views/partials/page-header.html'
    };
  });
