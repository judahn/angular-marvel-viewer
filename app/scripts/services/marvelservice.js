'use strict';

/**
 * @ngdoc service
 * @name marvelViewerApp.marvelService
 * @description
 * # marvelService
 * Factory in the marvelViewerApp.
 */
angular.module('marvelViewerApp')
  .factory('marvelService', function ($http) {

    var publicKey   = 'd51f28d3e9f44926a2891f563ce78e5c';
    var baseUrl     = 'http://gateway.marvel.com/v1/';
    var limit       = 48;

    var doRequest = function(category, offset) {
    	// console.log("do offset: ", offset, category);
    	
      // the $http API is based on the deferred/promise APIs exposed by the $q service
      // so it returns a promise for us by default

      var url = baseUrl + 'public/' + category + '?limit=' + limit + '&offset=' + (limit * offset) + '&apikey=' + publicKey;

      return $http.get(url)
        .then(function(response) {
          if (typeof response.data === 'object') {
            return response.data.data.results;
          } else {
            // invalid response
            return $q.reject(response.data);
          }
        }, function(response) {
          // something went wrong
          return $q.reject(response.data);
        });
    }

    return {
			load: function(category, offset) {
	    	return doRequest(category, offset);
	    }
    }
  });
