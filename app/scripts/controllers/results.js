'use strict';

/**
 * @ngdoc function
 * @name marvelViewerApp.controller:ResultsCtrl
 * @description
 * # ResultsCtrl
 * Controller of the marvelViewerApp
 */
angular.module('marvelViewerApp')
  .controller('ResultsCtrl', function ($scope, marvelService) {
    
    var offset = 0;
    
    var resultsCtrl = this;
        resultsCtrl.results = [];

    $scope.init = function(cat) {
        resultsCtrl.category = cat;
        resultsCtrl.load();
    };
    resultsCtrl.load = function(){
        // console.log("offset", offset);
        marvelService.load(resultsCtrl.category, offset)
          // then() called when son gets back
          .then(function(results) {
            // promise fulfilled
            // console.log(results[0]);
            resultsCtrl.results = results;
            
          }, function(error) {
              // promise rejected, could log the error with: console.log('error', error);
              // console.log('error');
          });
    }

    resultsCtrl.reset = function(){
        offset = 0;
        resultsCtrl.load();
    }
    resultsCtrl.back = function(){
        offset <= 0 ? offset = 0 : offset--;
        resultsCtrl.load();
    }
    resultsCtrl.next = function(){
        offset++;
        resultsCtrl.load();
    }
  });
