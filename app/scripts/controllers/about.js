'use strict';

/**
 * @ngdoc function
 * @name marvelViewerApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the marvelViewerApp
 */
angular.module('marvelViewerApp')
  .controller('AboutCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
